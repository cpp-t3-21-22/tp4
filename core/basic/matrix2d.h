//
// Created by beat on 06.12.16.
//

#ifndef TEST_MATRIX2D_H
#define TEST_MATRIX2D_H

template <class ValueType>
class Matrix2D                //mxn 2D Matrix with values of type "ValueType"
{
public:
    Matrix2D();                    // Default constructor, create an empty matrix 0x0
    Matrix2D(int line, int col);  // Constructor, create a 'line'x'col' empty matrix
    Matrix2D(const Matrix2D<ValueType>& m);  // Copy constructor
    ~Matrix2D();          // Destructor

    int getLines() const;  // get number of lines
    int getCols() const;   // get number of columns

    void random();                // Fill matrix random values
    void fill(ValueType v);       // Fill matrix with v
    void set(int line, int col, ValueType v);       // set the value at line,col
    ValueType get(int line, int col) const;  // set the value at line,col

    Matrix2D& operator=(const Matrix2D& m); // assignment operator for Matrix2D

    bool operator==(const Matrix2D& m) const; // equality test operator for Matrix2D
    bool operator!=(const Matrix2D& m) const; // equality test operator for Matrix2D
    Matrix2D operator+(const Matrix2D& m) const;// addition operator for Matrix2D
    Matrix2D operator-(const Matrix2D& m) const;// substraction operator for Matrix2D
    Matrix2D operator*(const Matrix2D& m) const; // multiply operator for Matrix2D

    void display() const;       // Display the matrix content
    void display(int n) const;  // Display the first N columns/lines partially the matrix content
};

#endif //TEST_MATRIX2D_H
