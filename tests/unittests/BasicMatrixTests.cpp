//
// Created by beat on 30.01.19.
//

#include "../../Catch2/catch.hpp"

#include "../../core/basic/matrix2d.h"
#include "../../core/rational/rat.h"

template <template<class> class MatrixType, class ValueType>
bool checkMatrixContent(MatrixType<ValueType> &matrix, ValueType v){
    for(int c = 0; c < matrix.getCols(); c++){
        for(int l = 0; l < matrix.getLines(); l++){
            REQUIRE(matrix.get(l, c) == v);
        }
    }
    return true;
}

template <template<class> class MatrixType, class ValueType>
void testEquality(MatrixType<ValueType> &matrix){

    SECTION("Equality test"){
        REQUIRE(matrix == matrix);
        REQUIRE(!(matrix != matrix));

        matrix.fill(3);
        MatrixType<ValueType> matrix2(matrix.getLines(), matrix.getCols());
        matrix2.fill(3);


        REQUIRE(matrix == matrix2);
        REQUIRE(!(matrix != matrix2));

        matrix2.fill(4);

        REQUIRE(matrix != matrix2);
        REQUIRE(!(matrix == matrix2));
    }

}

template <template<class> class MatrixType, class ValueType>
void testMatrixInit(MatrixType<ValueType> &matrix){
    SECTION("Zeroed"){
        matrix.fill(0);

        checkMatrixContent<Matrix2D, ValueType>(matrix, 0);
        testEquality(matrix);
    }

    SECTION("Random"){
        matrix.random();

        ValueType last = 0;
        int different = 0;
        for(int c = 0; c < matrix.getCols(); c++) {
            for (int l = 0; l < matrix.getLines(); l++) {
                if(matrix.get(l, c) != last){
                    different++;
                }
                last = matrix.get(l, c);
            }
        }

        REQUIRE(different >= matrix.getCols() * matrix.getLines() / 4);

        testEquality(matrix);
    }

    SECTION("Store and load"){
        for(int c = 0; c < matrix.getCols(); c++) {
            for (int l = 0; l < matrix.getLines(); l++) {
                matrix.set(l, c, l * c);
            }
        }

        for(int c = 0; c < matrix.getCols(); c++){
            for(int l = 0; l < matrix.getLines(); l++){
                REQUIRE(matrix.get(l, c) == l * c);
            }
        }

        testEquality(matrix);
    }
}

TEMPLATE_TEST_CASE( "1: Test matrix creation", "MatrixTests", int, double, Rat) {

    SECTION("Square") {
        for (int i = 1; i < 100; i++) {
            Matrix2D<TestType> matrix(i, i);

            REQUIRE(matrix.getCols() == i);
            REQUIRE(matrix.getLines() == i);

            testMatrixInit(matrix);
        }
    }

    SECTION("Rectangular"){
        for(int i = 1; i < 100; i++){

            for(int loop = 1; loop < 100; loop++){
                if(i != loop){
                    Matrix2D<TestType> matrix(i, loop);

                    REQUIRE(matrix.getLines() == i);
                    REQUIRE(matrix.getCols() == loop);

                    testMatrixInit(matrix);
                }
            }
        }
    }

}

TEMPLATE_TEST_CASE( "1: Test matrix calculations", "MatrixTests", int, double, Rat) {
    Matrix2D<TestType> a(3,3);
    Matrix2D<TestType> b(3,3);

    a.fill(1);
    b.fill(2);

    SECTION("Plus"){
        Matrix2D<TestType> c = a + b;

        SECTION("Size check"){
            REQUIRE(c.getCols() == 3);
            REQUIRE(c.getLines() == 3);
        }

        SECTION("Unchanged source"){
            REQUIRE(checkMatrixContent<Matrix2D, TestType>(a, 1));
            REQUIRE(checkMatrixContent<Matrix2D, TestType>(b, 2));
        }

        SECTION("Result"){
            REQUIRE(checkMatrixContent<Matrix2D, TestType>(c, 3));
        }

        SECTION("Randomness"){
            a.random();
            b.random();

            c = a + b;

            for(int col = 0; col < c.getCols(); col++) {
                for (int line = 0; line < c.getLines(); line++) {
                    REQUIRE(c.get(line, col) == (a.get(line, col) + b.get(line, col)));
                }
            }
        }
    }

    SECTION("Minus"){
        Matrix2D<TestType> c = a - b;

        SECTION("Result"){
            REQUIRE(checkMatrixContent<Matrix2D, TestType>(c, -1));
        }

        SECTION("Randomness"){
            a.random();
            b.random();

            c = a - b;

            for(int col = 0; col < c.getCols(); col++) {
                for (int line = 0; line < c.getLines(); line++) {
                    REQUIRE(c.get(line, col) == (a.get(line, col) - b.get(line, col)));
                }
            }
        }
    }
}

TEMPLATE_TEST_CASE( "1: Matrix multiplication", "MatrixTests", int, double, Rat) {
    Matrix2D<TestType> a(2,2);
    Matrix2D<TestType> b(2,2);

    SECTION("SQUARE"){

        a.set(0, 0, 1);
        a.set(0, 1, 2);
        a.set(1, 0, 3);
        a.set(1, 1, 4);

        b.set(0, 0, 2);
        b.set(0, 1, 0);
        b.set(1, 0, 1);
        b.set(1, 1, 2);

        Matrix2D<TestType> c = a * b;

        SECTION("Size check"){
            REQUIRE(c.getCols() == 2);
            REQUIRE(c.getLines() == 2);
        }

        SECTION("Results"){
            REQUIRE(c.get(0,0) == 4);
            REQUIRE(c.get(0,1) == 4);
            REQUIRE(c.get(1,0) == 10);
            REQUIRE(c.get(1,1) == 8);
        }
    }


    SECTION("RECTANGULAR"){
        a = Matrix2D<TestType>(3, 2);
        b = Matrix2D<TestType>(2, 3);

        a.fill(3);
        b.fill(2);

        Matrix2D<TestType> c = a * b;

        SECTION("Size check"){
            REQUIRE(c.getCols() == 3);
            REQUIRE(c.getLines() == 3);
        }

        SECTION("Results"){
            REQUIRE(checkMatrixContent<Matrix2D, TestType>(c, 12));
        }
    }

}

TEMPLATE_TEST_CASE( "1: Advanced tests", "MatrixTests", int, double, Rat) {
    Matrix2D<TestType> a(2,2);
    Matrix2D<TestType> b(2,2);

    a.fill(1);
    b.fill(2);

    Matrix2D<TestType> c = (a * b) - (a + b * b) + a * a;

    REQUIRE(checkMatrixContent<Matrix2D, TestType>(c, -3));
}